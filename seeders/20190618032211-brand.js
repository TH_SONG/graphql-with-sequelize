'use strict';

const util = require('../helper/util');

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('brands', [
            {
                id: 1,
                name: "HYUNDAI",
                intro: "We exist to enhance people’s lives.\n" +
                "Because we believe that life’s most remarkable moments and experiences\n" +
                "Weren’t meant for a select few, but for all.\n" +
                "\n" +
                "We think beyond product and technology to create meaningful experiences. \n" +
                "How?\n" +
                "By challenging the standards of design and workmanship,\n" +
                "Leading a new era of customer care,\n" +
                "And rethinking the future of mobility.\n" +
                "Looking forward,\n" +
                "We believe in a world where mobility will enhance life\n" +
                "And will be defined by people, for people.\n" +
                "With our unyielding can-do spirit,\n" +
                "We will be steadfast in pursuit of this vision,\n" +
                "For we do not seek to satisfy the few,\n" +
                "But to be loved by many.",
                type: "interior",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 2,
                name: "KIA",
                intro: "At Kia, we're inspired by the idea that you should love driving and owning your car. Designing, engineering, building, and bringing you that car is what we do. And we love what we do. Turns out we're not alone. Auto experts and everyday drivers love what we do, too. The 2017 Sportage won the Red Dot Award for Product Design in 2016. The 2016 Optima received Kelley Blue Book's KBB.com Midsize Car Best Buy Award. The word is out. The superlatives are gratifying. Yet we're determined to keep evolving. \n" +
                "\n" +
                "To that end, Kia has been making news with a remarkable run of firsts. We introduced the superb K900, our first rear-drive, full-size luxury sedan. We debuted the Soul EV, our first all-electric, zero-emissions vehicle. The soon to come 2017 Optima Plug-In Hybrid is our first vehicle of its kind. The exciting firsts go on and on. From design to technology, materials to safety features, Kia continues to innovate and upgrade our entire lineup.\n" +
                "\n" +
                "You deserve to own a car you love. We invite you to discover the “New Kia.”",
                type: "interior",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 3,
                name: "MERCEDES BENZ",
                intro: "Gottlieb Daimler is born on 17 March 1834 in Schorndorf. After training as a gunsmith and working in France, he attends the Polytechnic School in Stuttgart from 1857 to 1859. After completing various technical activities in France and England, he starts work as a draftsman in Geislingen in 1862. At the end of 1863, he is appointed workshop inspector in a machine tool factory in Reutlingen, where he meets Wilhelm Maybach in 1865. In 1872, he becomes Technical Director of the gas engine manufacturer Deutz Gasmotorenfabrik, where he becomes familiar with Otto’s four-stroke technology. After differences with the Managing Director, he leaves the company in 1882. Daimler sets up a development workshop in his greenhouse at his Cannstatt villa to concentrate on developing petrol-driven four-stroke engines. Working with Wilhelm Maybach in 1884, he develops an internal combustion engine known today as the 'Grandfather Clock'. With its compact, low-weight design, the machine forms the basis for installation in a vehicle. The costs of trial operations soon consume Daimler’s entire fortune, however, so he is obliged to find business partners. He founds 'Daimler-Motoren-Gesellschaft' on 28 November 1890 together with Max Duttenhofer and his business partner Wilhelm Lorenz. But while Duttenhofer wants to produce stationary engines, Daimler prefers to focus on vehicle production, and a dispute ensues.\n" +
                "\n" +
                "After Wilhelm Maybach resigns in 1891 due to unacceptable terms of contract, Daimler resorts to a ruse. He continues to build engines with Maybach, but the patents are all in his name. The increasingly tense relationship with Duttenhofer and Lorenz lead them to exclude Daimler as a shareholder. A deterioration in finances leads to stagnating technical development, which prompts DMG to attempt to reinstate Maybach in 1895. He refuses, pointing out that he would not come back without Daimler. In the end, commercial pressures result in both of them returning to the company. Thanks to the Phoenix engine built by Maybach, the Daimler engine gains popularity abroad. A group of English industrialists are prepared to pay 350,000 Marks for licensing rights. Maybach is appointed Technical Director of DMG, and Gottlieb Daimler receives a position on the Technical Board and becomes the Inspector General of the Supervisory Board. The return of both men to DMG is an unexpected boost for the company. Gottlieb Daimler enjoys this rapid development for only a short period of time. He dies of heart disease on 6 March 1900.",
                type: "exterior",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 4,
                name: "AUDI",
                intro: "Discover Audi as a brand, company and employer on our international website. Here you will find information about models and technologies. Inspiring content, interesting backgrounds and fascinating moments — digital, individual and authentic. Experience our vision of mobility and let yourself be inspired.",
                type: "exterior",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 5,
                name: "BMW",
                intro: "Automated driving, electro-mobility, on-demand mobility and connectivity-mobility have never been so fascinating – and automobile advancement never so exciting and promising – as it is today. With BMW.com, the international website for BMW, we would like to create a platform that brings you closer to this fascination and the latest technological trends. With content focusing on topics like mobility, stories of past and recent milestones, facts about historic, recent and future BMW models.\n" +
                "\n" +
                "Dive into these new worlds with us, get inspired, and experience something new, unusual and helpful every day. At BMW.com – our digital interpretation of the sheer pleasure of driving.",
                type: "exterior",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            },
            {
                id: 6,
                name: "RENAULT SAMSUNG",
                intro: "Our group, which has been making cars since 1898, is present in 134 countries and has sold 3.9 million vehicles in 2018. To meet the major technological challenges of the future while continuing to pursue our profitable growth strategy, we are focusing on international expansion and drawing on the synergies of our five brands: Renault, Dacia, Renault Samsung Motors, Alpine and LADA.",
                type: "interior",
                created_at: util.getNow(),
                updated_at: util.getNow(),
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('brands', null, {});
    }
};
