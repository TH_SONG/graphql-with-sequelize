import Sequelize, { Model } from 'sequelize';

class User extends Model {
    static tableName = 'users';

    static associate(models) {
        // User.Validation = User.hasMany(models.Validation, {
        //     foreignKey: 'user_id',
        //     as : 'validation'
        // });
    }
}

const schema = {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
    },
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    name: Sequelize.STRING,
    type: Sequelize.ENUM('bcn', 'kakao'),
};

export default (sequelize) => {
    User.init(schema, {
        sequelize,
        tableName: User.tableName,
        timestamps: true,
        underscored: true,
        paranoid: true,
    });

    return User;
};
